/**
 * Fetch all products
 */
const query = `
query {
 visitor(token:"") {
   store(market: SWEDEN, locale: SV_SE) {
     ... on Store_Found {
       getProducts(first:200) {
         totalCount
         edges {
           node {
             typeId
             description
             shortDescription
             webExclusive
             categories {
               edges {
                 node {
                   id
                   name
                   path
                 }
               }
             }
             attributes {
               optionLabel
             }
             ... on Product_Simple {
              id
              sku
              name
              stockAmount
              stockStatus
              price {
                currency
                value
              }
              profit {
                currency
                value
              }
             }
             ... on Product_Configurable {
               id
               sku
               name
               children {
                 ... on Product_Simple {
                  id
                  sku
                  name
                  stockAmount
                  stockStatus
                  price {
                    currency
                    value
                  }
                  profit {
                    currency
                    value
                  }
                 }
               }
               configuration {
                 id
                 code
                 label
                 options {
                   id
                   label
                   sortPosition
                 }
               }
             }
           }
         }
       }
     }
   }
 }
}
`;

/**
 * Place quote
 */
const gqlQuery = `mutation {
    visitor(token:"") {
      store(market:SWEDEN, locale: SV_SE) {
        ... on Store_Found {
          placeQuote(input: {
            sellerId:"${sellerId}"
            email: "${email}"
            firstName:"${firstName}"
            lastName:"${lastName}"
            telephone:"${phone}"
            address:"${address}, ${postalCode.replace(" ", "")} ${city}"
            subscribe:${newsletter}
            products:[
              ${items.map((item) => {
                const productId = item.configurationId
                  ? item.configurationId
                  : item.id;
                return `
                  {
                    productId:"${productId}"
                    qty:${item.quantity}
                    salesChannel: SELLER_LINK
                  }
                `;
              })}
            ]
          }) {
            ... on PlaceQuoteResult_Success {
              id
            }
            ... on PlaceQuoteResult_Error {
              message
            }
          }
        }
      }
    }
  }`;

/**
 * Place Order
 */
const gqlQuery = `mutation {
    visitor(token:"") {
      store(market:SWEDEN, locale: SV_SE) {
        ... on Store_Found {
          placeOrder(input: {
            sellerId:"${sellerId}"
            email: "${email}"
            firstName:"${firstName}"
            lastName:"${lastName}"
            telephone:"${phone}"
            street:"${address}"
            postcode:"${postalCode.replace(" ", "")}"
            city:"${city}"
            subscribe:${newsletter}
            products:[
              ${items.map((item) => {
                const productId = item.configurationId
                  ? item.configurationId
                  : item.id;
                return `
                  {
                    productId:"${productId}"
                    qty:${item.quantity}
                    salesChannel: ${sellerLink ? "SELLER_LINK" : "WEBSHOP"}
                  }
                `;
              })}
            ]
            shippingMethod: "${selectedShipping}"
            paymentMethod:${transactionId}
          }) {
            ... on PlaceOrderResult_Success {
              id
            }
            ... on PlaceOrderResult_Error {
              message
            }
          }
        }
      }
    }
  }`;

/**
 * Shop products on catalog
 * for availability and stock
 */
const query = `query {
    visitor(token:"") {
      store(market:SWEDEN, locale: SV_SE) {
        ... on Store_Found {
          getShopById(id:"${shopId}") {
            catalog {
              name
              products {
                edges {
                  node {
                    id
                    name
                    sku
                    stockStatus
                    ... on Product_Simple {
                      stockAmount
                    }
                    ... on Product_Configurable {
                      children {
                        id
                        sku
                        name
                        stockStatus
                        stockAmount
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }`;

/**
 * Shipping
 */
const gqlQuery = `{
    visitor(token:"") {
      store(market:SWEDEN, locale: SV_SE) {
        ... on Store_Found {
          getShippingMethods(input: {
              sellerId:"${sellerId}"
              email: "${email}"
              firstName:"${firstName}"
              lastName:"${lastName}"
              telephone:"${phone}"
              street:"${address}"
              postcode:"${postalCode.replace(" ", "")}"
              city:"${city}"
              subscribe:false
              products:[
                ${items.map((item) => {
                  const productId = item.configurationId
                    ? item.configurationId
                    : item.id;
                  return `
                    {
                      productId:"${productId}"
                      qty:${item.quantity},
                      salesChannel: ${sellerLink ? "SELLER_LINK" : "WEBSHOP"}
                    }
                  `;
                })}
              ]
          }) {
            title
            description
            price {
              value
              currency
            }
             ... on ShippingMethod_Other {
              rates {
                id
                title
              }
            }
            ... on ShippingMethod_Bring {
              rates {
                id
                title
                date
                timeFrom
                timeTo
              }
            }
          }
        }
      }
    }
  }`;

/**
 * Search Shop
 */
let query = "";
if (searchQuery) {
  query = `name:"${searchQuery.toLowerCase()}"`;
}
if (latitude && !searchQuery) {
  query = `location: {distance:10 latitude:${latitude} longitude: ${longitude}}`;
}
const gqlQuery = `
   query {
     visitor(token: "") {
       store(market: SWEDEN, locale: SV_SE) {
         ... on Store_Found {
           getShops(first: 10, ${query}) {
             edges {
               node {
                 id
                 shopName
                 organisationName
                 managerId
                 logo {
                   url
                 }
               }
             }
           }
         }
       }
     }
   }
   `;

/**
 * Fetch categories
 */
const query = `
    {
      visitor(token:"") {
        store(market:SWEDEN, locale: SV_SE) {
          ... on Store_Found {
            getCategories(first:100) {
              edges {
                node {
                  id
                  name
                  path
                }
              }
            }
          }
        }
      }
    }
    `;
