# Mastershop

## Filer
1. [api.md](/api.md) - API skiss 
2. [mock.js](/mock.js) - Mockade objekt
3. [schema.graphql](/schema.graphql) - Förslag på Graphql schema
4. [struktur.png](/struktur.png) - Katalogskiss
5. [Wireframes](https://www.figma.com/file/8uaGTuGpmmZEqBosRDNIRC/Design%2C-Fas-2%2C-Newbody%2C-Kundkopia?node-id=31%3A0) - Wireframes i Figma (Obs. mer funktioner än nuvarande scope)

## Introduktion
Upplägget är att via en storefront-applikation kommunicera via en så kallad API Gateway som i sin tur kommunicerar med andra system, däribland nuvarande Magento 1 (https://www.newbody.se)

1. newbodyfamily.com/sv-se/shop
2. api.newbodyfamily.com
3. newbody.se

API Gatewayen tar Enfo fram i verktyget [tyk.io](https://tyk.io). Enfo kommer även att bygga ett såkallad transformationslager som översätter API:förfrågningarna mellan systemen så att de plattformar som kommunicerar med API Gateway inte behöver uppdateras vid förändringar.

Everest står för vidareutveckling av NAV för att stödja de nya kraven.

Webbhuset vidarutvecklar de externa API:et från Magento.


## Struktur
Nedan presenteras föreslaget på struktur för de olika katalogerna samt koncepten.

![alt text](/struktur.png "Struktur")

Kortfattat så vill man på sikt bara ha ett koncept men just nu hindrar t.ex Spicy Dreams oss från att göra det direkt p.g.a interna logistiska svårigheter.

**Mastershop**

Via mastershoppen får man bara tillgång till produkter från mastershoppens katalog samt de webbexlusiva produkterna. 
Här söker man fram ett lag man vill stödja som sedan får förtjänsten efter direktbetalning.

**Säljarlänk - NB**

Via säljarlänk kommer man in på en säljares sida som har tillgång till flera kataloger mastershop + katalog + webbexlusiva produkter.

**Säljarlänk Spicy**

Via säljarlänk kommer man in på mastershoppen fast utformat för konceptet Spicy Dream. Man kommer bara ha tillgång till katalogens sortiment + webbexlusiva produkter. 

## Orderläggning
Kommer man via mastershoppen och handlar så läggs det precis som idag ordrar direkt som ska skickas in i plockflödet.
Kommer man däremot via säljarlänk så ska man precis som idag fortfarande ha möjligheten att välja att få sina produkter levererade via säljaren, och ordern ska skapas upp som en beställning som kommer med i den stora ordern efter avslutat försäljningsperiod.

## Betallösning
För maximal flexibilitet kommer storefrontent sköta betalningsflödet, och vid färdigt köp uppdatera ordern med betalningsinformation.

## Vad vi behöver kunna göra
**Produkter**

- Hämta ut produkter, filtrera på parametrar
- Får reda på lagerstatus för produkt(er)
- Få ut information om t.ex bidragsvolym per produkt

**Kategorier**

- Hämta ut kategorier, filtrera på parametrar

**Shoppar**

- Hämta specifik shop och säljare efter parametrar
- Söka fram shop efter namn, stad, land osv.

**Katalog**

Om man ser mastershoppen som en katalog, säljares produkter som en katalog samt de webbexlusiva som en katalog.
Önskemål hade varit att anropa `/catalogs/[mastershopID,shopID,webbexclusiveID]` och få ut produkter som matchar dessa kataloger och dess lagersaldo
- Hämta ut katalog och dess produkter utifrån parametrar

**Order / Beställning**

- Skapa upp order
- Uppdatera order efter köp med betalningssinformation
- Hämta leveransmöjligheter utefter valda produkter ( Centiro )
- På orderrad specificera avräkningskatalog


## Framtid
Som sagt så finns en ambition att gå mot ett koncept och då ha möjligheten till flera varumärken som beställare. 
Det finns även tankar om att möjliggöra för slutkunder att kunna logga in och få tillgång till historik av ordrar och annan värdefull information (Mina sidor).



