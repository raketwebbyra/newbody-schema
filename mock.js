/**
 * Constants - START
 */

export const DATE_TIME = "2021-01-01 10:10:10";
export const PLACEHOLDER_IMAGES = {
  SMALL: "http://via.placeholder.com/150x150",
  MEDIUM: "http://via.placeholder.com/600x600",
};

export const MARKETS = {
  SWEDEN: "SWEDEN",
  FINLAND: "FINLAND",
};

export const LOCALES = {
  SV_SE: "SV_SE",
  SV_FI: "SV_FI",
  FI_FI: "FI_FI",
};

export const BRANDS = {
  NEWBODY: "NEWBODY",
  SPICY_DREAM: "SPICY_DREAM",
  HOME_SEREMONIES: "HOME_SEREMONIES",
};

export const SHOP_STATUS = {
  INACTIVE: "INACTIVE",
  PREPARING: "PREPARING",
  WAITING: "WAITING",
  ACTIVE: "ACTIVE",
  EXTENDED: "EXTENDED",
  COMPLETE: "COMPLETE",
  ARCHIVED: "ARCHIVED",
};

export const SALES_STATUS = {
  IN_STOCK: "IN_STOCK",
  OUT_OF_STOCK: "OUT_OF_STOCK",
  DISCONTINUED: "DISCONTINUED",
};

export const PRICE_DISPLAY_TYPE = {
  NORMAL: "NORMAL",
  FROM: "FROM",
  RANGE: "RANGE",
};

export const CURRENCY_SYMBOL_POSITION = {
  AFTER: "AFTER",
  BEFORE: "BEFORE",
};
/**
 * Constants - END
 */

/**
 * Currency
 */
export const currency = {
  code: "SEK",
  symbol: "kr",
  symbolPosition: CURRENCY_SYMBOL_POSITION.AFTER,
};

/**
 * Product Price
 */
export const productPrice = {
  price: 159,
  displayType: PRICE_DISPLAY_TYPE.NORMAL,
  currency: PRICE_DISPLAY_TYPE.NORMAL,
};
/**
 * Image
 */
export const image = {
  caption: "Image caption",
  sizes: [imageFile],
  tags: ["Tag 1", "Tag 2"],
};

export const imageFile = {
  src: PLACEHOLDER_IMAGES.MEDIUM,
  width: 600,
  height: 600,
};

/**
 * Category
 */
export const category = {
  id: 1,
  name: "Kläder",
  parentId: 0,
  products: [],
};

/**
 * Organization
 */
export const organization = {
  id: 1,
  name: "Organization 1",
  logo: PLACEHOLDER_IMAGES.SMALL,
  image: PLACEHOLDER_IMAGES.MEDIUM,
};

/**
 * Catalog
 */
export const catalog = {
  id: 1,
  name: "Organization 1",
  products: [],
};

/**
 * Stock status
 */
export const stockStatus = {
  quantity: 10,
  catalogId: 1,
};

/**
 * Product
 */
export const product = {
  id: 1,
  typeCode: "clothing",
  sku: "123",
  name: "Produkt 1",
  salesStatus: SALES_STATUS.IN_STOCK,
  price: productPrice,
  shortDescription: "Short description",
  description: "Long Description",
  featuredImage: image,
  images: [image],
  categories: [category],
  brand: BRANDS.NEWBODY,
  stockStatus: [stockStatus],
};

/**
 * Shop
 */
export const shop = {
  id: 1,
  name: "Shop 1",
  description: "Description",
  logo: PLACEHOLDER_IMAGES.SMALL,
  image: PLACEHOLDER_IMAGES.MEDIUM,
  organization: organization,
  startPackage: product,
  catalog: catalog,
  startDate: DATE_TIME,
  endDate: DATE_TIME,
  extendedDate: DATE_TIME,
  market: MARKETS.SWEDEN,
  status: SHOP_STATUS.ACTIVE,
  brand: BRANDS.NEWBODY,
  url: "#",
};

export const item = {
  id: 1,
  sku: "123",
  name: "Product 1",
  quantity: 1,
  price: 159,
  product: product,
  catalogId: 1,
};

export const address = {
  id: 1,
  firstName: "Namn",
  lastName: "Namnsson",
  street: "Street 1",
  postalCode: "54159",
  city: "Göteborg",
  phone: "0706644356",
  region: "",
  company: "",
  county: "SE",
};

export const shippingMethod = {
  id: 1,
  label: "Hämta på vårt lager i Sisjön",
  description: "Leveranstid 2-5 dagar",
  price: 69,
};

export const shipment = {
  title: "Postnord",
  carrier: "Postnord",
  shipmentId: 1,
  trackingNumber: 1,
  trackingLink: "#",
  estimatedDeliveryDate: DATE_TIME,
  shippedDate: DATE_TIME,
  items: [item],
};

/**
 * Order
 */
export const order = {
  id: 1,
  createdDate: DATE_TIME,
  paymentMethod: "klarna",
  transactionId: "00000000-0000-0000-0000-000000000000",
  status: "pending",
  shopId: 1,
  sellerId: 1,
  email: "email@email.com",
  items: [item],
  shippingAddress: address,
  billingAddress: address,
  selectedShippingMethod: "x",
  shippingMethods: [shippingMethod],
  shipment: shipment,
  locale: LOCALES.SV_SE,
};
