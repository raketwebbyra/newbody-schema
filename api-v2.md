## Endpoints
```json
// All products
[GET] /products
[PARAMS] ids, sku, brand, market, locale, limit

// Single product
[GET] /products/{sku || id}
[PARAMS] market, locale

// All categories
[GET] /categories
[PARAMS] market, locale

// Single category
[GET] /categories/{id}
[PARAMS] market, locale

// Single catalog (Catalog X)
[GET] /catalog/{id}
[PARAMS] market, locale
[AVAILABLE_SUB_DATA] products

// All shops (for search)
[GET] /shops
[PARAMS] query, latitude, longitude, market, locale

// Single shop
[GET] /shops/{shopId}
[PARAMS] sellerId, market, locale
[AVAILABLE_SUB_DATA] products, seller

// Create order
[POST] /orders
[PARAMS] market, locale

// Get shipping options
[POST] /shipping
[PARAMS] market, locale

// Create quote
[POST] /quotes
[PARAMS] market, locale

```
