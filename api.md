## Endpoints
```json
// All products
[GET] /products
[PARAMS] query, shop, catalog, brand, sku, market, limit, page
// Single product
[GET] /products/{sku}
[PARAMS] shop, catalog
// Stock status for products
[GET] /products/stock-status
[PARAMS] sku, shop, catalog

// All categories
[GET] /categories
[PARAMS] brand, catalog
// Single category
[GET] /categories/{id}

// Single catalog (Master shop, Catalog X)
[GET] /catalog/{id}

// All shops (for search)
[GET] /shops
[PARAMS] query, city, country, limit, page, brand, market
// Single shop
[GET] /shops/{id}
[PARAMS] seller

// Create order
[POST] /orders
// Update order
[PUT] /orders/{id}
// Get order
[GET] /orders/{id}
```
