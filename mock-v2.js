/**
 * Product Data
 */
const product = {
  id: 1,
  name: "Product Name",
  sku: "sku",
  stockAmount: 100, // Simple product...
  stockStatus: "IN_STOCK",
  webExclusive: false,
  price: {
    currency: 'SEK',
    value: 159
  },
  revenue: {
    currency: 'SEK',
    value: 159
  },
  shortDescription: "Short description",
  description: "Description",
  categories: [
    {
      id: 8,
      name: "Herr",
      parentIds: []
    }
  ],
  configuration:[
    {
      type: 'size',
      options: [
        {
          label: "M",
          value: "sku / id"
        }
      ]
    }
  ],
  brand: "NEWBODY",
  children: [
    {
      id: 2,
      name: "Child Product",
      sku: "1234",
      stockAmount: 1000,
      stockStatus: "IN_STOCK",
      webExclusive: false,
      price: {
        currency: 'SEK',
        value: 159
      },
      revenue: {
        currency: 'SEK',
        value: 159
      },
      shortDescription: "Short description",
      description: "Description",
      categories: [
        {
          id: 8,
          name: "Herr",
          parentIds: []
        }
      ],
      brand: "NEWBODY",
    }
  ]
}

/**
 * Category
 */
 const category = {
  id: 8,
  name: "Herr",
  parentIds: []
}

/**
 * Shop
 */
const shop = {
  id: 1,
  name: "Name",
  organizationName: "Organization Name",
  managerId: 123,
  description: "Description",
  logo: "http:image",
  brand: "NEWBODY",
  // Only single fetch 
  seller: {
    id: 1,
    name: "Name Namesson"
  },
  products: []
}

/**
 * Catalog
 */
 const catalog = {
  id: 1,
  name: "Name",
  brand: "NEWBODY",
  products: []
}

/**
 * Shipping
 */
 const shippingPostData = {
  sellerId: 1,
  address: {
    firstName: "Namn",
    lastName: "Namnsson",
    street: "Street 1",
    postalCode: "54159",
    city: "Göteborg",
    phone: "0706644356",
  },
  products: [
    {
      id: "123",
      quantity: 2,
      salesChannel: "WEBSHOP"
    }
  ]
}
const shippings = [
  {
    title: "Bring",
    description: "",
    price: {
      currency: "SEK",
      value: 69
    },
    options: [
      {
        title: "Eklanda Godis",
        value: "bring_weird_word",
        date: "2021-04-21",
        timeFrom: "20:00",
        timeTo: "21:00",
      }
    ]
  }
]

/**
 * Order / Quote
 */
 const order = {
  sellerId: 1,
  selectedShipping: "",
  transactionId: "",
  newsletter: true,
  shippingAddress: {
    firstName: "Namn",
    lastName: "Namnsson",
    street: "Street 1",
    postalCode: "54159",
    city: "Göteborg",
    phone: "0706644356",
  },
  products: [
    {
      id: "123",
      quantity: 2,
      salesChannel: "WEBSHOP"
    }
  ]
}